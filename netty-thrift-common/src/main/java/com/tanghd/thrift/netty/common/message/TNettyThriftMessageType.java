package com.tanghd.thrift.netty.common.message;

public enum TNettyThriftMessageType {
    FRAMED, UNFRAMED;
}
